def reverser
  yield.split.map { |word| word.reverse}.join(" ")
end

def adder(num = 1)
  return yield + num
end

def repeater(num_repeats = 1)
    num_repeats.times { |n| yield }
end
